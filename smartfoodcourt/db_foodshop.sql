-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 04, 2020 lúc 07:41 PM
-- Phiên bản máy phục vụ: 10.4.13-MariaDB
-- Phiên bản PHP: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `db_coffeshop`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `abouts`
--

INSERT INTO `abouts` (`id`, `title`, `content`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Về chúng tôi', 'Chúng tôi là Smart Food Court', 1, '2018-05-14 09:58:57', '2020-06-16 19:34:56');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` tinyint(4) NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `admins`
--

INSERT INTO `admins` (`id`, `user_name`, `roles`, `password`, `email`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(5, 'admin', 1, '$2y$10$o0536xrgEK55EUZJorcju.pzFXmgwjkblc2pqEYtg24QZra7fubcq', 'admin@gmail.com', 1, '1nt60yAZzivjPhHHCN5WYSY38Ov4tu7oWWiAXARbw0OfdR4Io7MghXtL5E7H', '2018-06-26 11:28:43', '2018-06-26 11:28:43');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category_new`
--

CREATE TABLE `category_new` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category_new`
--

INSERT INTO `category_new` (`id`, `name`, `sort`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Tin tức và Sự kiện', 1, 1, '2020-06-16 09:12:52', '2020-06-16 09:12:52'),
(2, 'Tin khuyến mại', 2, 1, '2020-06-16 09:13:10', '2020-06-16 09:13:10'),
(3, 'Tin tức sản phẩm', 3, 1, '2020-06-16 19:34:02', '2020-06-16 19:34:02');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category_product`
--

CREATE TABLE `category_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` tinyint(4) NOT NULL,
  `orders` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `category_product`
--

INSERT INTO `category_product` (`id`, `name`, `parent_id`, `orders`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Cơm', 0, 1, 1, '2020-06-16 08:20:57', '2020-06-16 08:20:57'),
(2, 'Đồ nước', 0, 2, 1, '2020-06-16 08:21:13', '2020-06-16 08:21:13'),
(3, 'Đồ ăn sáng', 0, 3, 1, '2020-06-16 08:21:51', '2020-06-16 08:21:51'),
(4, 'Nước uống', 0, 4, 1, '2020-06-16 08:21:56', '2020-06-16 08:21:56');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_03_27_155711_create_category_product_table', 1),
(4, '2018_03_27_163233_create_admins_table', 1),
(5, '2018_03_27_165531_create_products_table', 1),
(6, '2018_03_27_172957_create_suppliers_table', 1),
(7, '2018_03_27_175642_create_slides_table', 1),
(8, '2018_03_27_175948_create_category_new_table', 1),
(9, '2018_03_27_180546_create_news_table', 1),
(10, '2018_03_27_181645_create_transaction_table', 1),
(11, '2018_03_27_184310_create_orders_table', 1),
(12, 'SuppllersTableSeeder', 1),
(13, 'SlideTableSeeder', 1),
(14, 'CategoryProductTableSeeder', 1),
(15, 'ProductsTableSeeder', 1),
(16, 'CategoryNewsTableSeeder', 1),
(17, 'NewsTableSeeder', 1),
(18, '2018_05_14_151501_create_abouts_table', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cate_new_id` int(11) NOT NULL,
  `info` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` bigint(20) NOT NULL,
  `amount` bigint(20) NOT NULL,
  `delivery_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `transaction_id`, `product_id`, `qty`, `product_name`, `price`, `amount`, `delivery_date`, `created_at`, `updated_at`) VALUES
(5, 6, 5, 1, 'Bún bò huế', 25000, 25000, NULL, '2020-07-30 00:12:19', '2020-07-30 00:12:19');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `price` bigint(15) NOT NULL,
  `sale` int(11) DEFAULT NULL,
  `show_home` tinyint(4) NOT NULL,
  `hot` tinyint(4) NOT NULL,
  `view` int(11) DEFAULT NULL,
  `buyed` int(11) DEFAULT 0,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thunbar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `name`, `category_id`, `price`, `sale`, `show_home`, `hot`, `view`, `buyed`, `image`, `unit`, `thunbar`, `description`, `content`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Cơm thịt luộc', 1, 25000, 0, 1, 0, 2, 0, 'foody-mobile-o-jpg-383-635932169710472456.jpg', NULL, NULL, '', NULL, 1, '2020-06-16 08:29:49', '2020-07-29 23:40:08'),
(2, 'Cơm gà ', 1, 25000, NULL, 1, 1, 3, 0, 'foody-mobile-t2-jpg-547-635984903461195242.jpg', NULL, NULL, '', NULL, 1, '2020-06-16 08:31:09', '2020-07-29 23:40:14'),
(3, 'Cơm cá kho tộ', 1, 30000, NULL, 1, 1, 12, 1, 'foody-mobile-xe9qrkf5-jpg-117-636070269554128177.jpg', NULL, NULL, '', NULL, 1, '2020-06-16 08:31:38', '2020-06-16 09:22:23'),
(4, 'Cơm sườn nướng', 1, 25000, NULL, 1, 1, 7, 1, 'foody-mobile-t2-jpg-405-635948494587632721.jpg', NULL, NULL, '', NULL, 1, '2020-06-16 08:33:06', '2020-07-29 23:42:26'),
(5, 'Bún bò huế', 2, 25000, 0, 1, 1, 2, 0, 'foody-mobile-foody-checkin-bun-bo-924-636020086030785958.jpg', NULL, NULL, '', NULL, 1, '2020-06-16 08:58:31', '2020-07-30 00:11:44'),
(6, 'Mì quảng tôm thịt', 2, 25000, NULL, 1, 0, 1, 0, 'foody-mobile-t2-jpg-361-635960710434073381.jpg', NULL, NULL, '', NULL, 1, '2020-06-16 08:59:14', '2020-06-16 09:27:27'),
(7, 'Hủ tiếu mực', 2, 25000, NULL, 1, 0, NULL, 0, 'foody-upload-api-foody-mobile-no2-jpg-180710162520.jpg', NULL, NULL, '', NULL, 1, '2020-06-16 09:06:31', '2020-06-16 09:06:47'),
(8, 'Bánh mì', 3, 10000, NULL, 1, 0, 1, 0, 'foody-mobile-z1-jpg-330-635763486454077110.jpg', NULL, NULL, '', NULL, 1, '2020-06-16 09:11:12', '2020-06-16 09:27:06'),
(9, 'Súp cua', 3, 15000, 0, 1, 1, NULL, 0, 'foody-upload-api-foody-mobile-2-200511172857.jpg', NULL, NULL, '', NULL, 1, '2020-06-16 09:12:08', '2020-06-16 09:12:08'),
(10, 'Cà phê sữa đá', 4, 12000, NULL, 1, 1, NULL, 0, 'foody-upload-api-foody-mobile-cafe-wrfdg-jpg-180612171625.jpg', NULL, NULL, '', NULL, 1, '2020-06-16 09:24:06', '2020-06-16 09:24:06'),
(11, 'Nước mía', 4, 5000, NULL, 1, 1, 4, 3, 'foody-upload-api-foody-mobile-hkmm-jpg-180806145047.jpg', NULL, NULL, '', NULL, 1, '2020-06-16 09:25:05', '2020-06-16 19:36:30'),
(12, 'Sinh tố bơ', 4, 15000, NULL, 1, 0, NULL, 0, 'foody-upload-api-foody-mobile-sinh-to-bo-jpg-180621093411.jpg', NULL, NULL, '', NULL, 1, '2020-06-16 09:25:50', '2020-06-16 09:26:33');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slides`
--

CREATE TABLE `slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `slides`
--

INSERT INTO `slides` (`id`, `name`, `image`, `link`, `sort`, `status`, `created_at`, `updated_at`) VALUES
(1, 'slide 1', 'ykRw6x.jpg', 'link 1', 1, 1, '2020-06-16 15:17:44', '2020-06-16 15:17:44'),
(2, 'slide 2', 'sxjKir.jpg', 'link 2', 2, 1, '2020-06-16 15:17:44', '2020-06-16 15:17:44'),
(3, 'slide 3', 'JDzNlb.jpg', 'link 23', 3, 1, '2020-06-16 15:17:44', '2020-06-16 15:17:44');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `transaction`
--

CREATE TABLE `transaction` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` bigint(20) DEFAULT NULL,
  `payment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transport` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `shiper` int(11) DEFAULT NULL,
  `notification` tinyint(2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `transaction`
--

INSERT INTO `transaction` (`id`, `user_id`, `name`, `email`, `phone`, `address`, `amount`, `payment`, `transport`, `message`, `status`, `shiper`, `notification`, `created_at`, `updated_at`) VALUES
(6, NULL, 'huy loc', 'huytanha@gmail.com', '0916199854', 'ktxB, DHBK', 25000, '1', '2', NULL, 0, NULL, NULL, '2020-07-30 00:12:19', '2020-07-30 00:12:19');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sex` tinyint(4) DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_user_name_unique` (`user_name`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Chỉ mục cho bảng `category_new`
--
ALTER TABLE `category_new`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `category_product`
--
ALTER TABLE `category_product`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Chỉ mục cho bảng `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `category_new`
--
ALTER TABLE `category_new`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `category_product`
--
ALTER TABLE `category_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
