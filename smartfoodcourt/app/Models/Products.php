<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    //
    const FOLDER_UPLOAD = 'uploads/admin/products';
    protected $table = 'products';

    protected $fillable = [
    	'name',
    	'category_id',
    	'price',
    	'sale',
    	'show_home',
    	'hot',
    	'view',
    	'buyed',
    	'image',
    	'thunbar',
    	'description',
    	'content',
    	'status',
    ];
    /**
     *
     * @var bool
     */
    public $timestamps = true;
}
