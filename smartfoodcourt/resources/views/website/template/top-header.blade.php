<div class="container">
	<div class="pull-left">
		<span><h2 style="color:black;">Chào mừng bạn đến Smart Food Court của trường ĐH Bách Khoa TPHCM !</h2></span>
	</div>
	<div class="pull-right">
		<ul class="header-top-links">
			@if(Session::has('users'))
				<?php $users = Session::get('users'); ?>
				<li><a href="#">Xin chào : {{ $users['name'] }}</a></li>
			@endif

		</ul>
	</div>
</div>