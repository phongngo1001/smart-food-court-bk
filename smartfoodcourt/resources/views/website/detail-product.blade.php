@extends('website.index')
@section('title', 'Sản phẩm')
@section('show', 'show-on-click')

@section('content')
	<!-- section -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!--  Product Details -->
				<div class="product product-details clearfix">
					<div class="col-md-6">
						<div id="product-main-view">
							<div class="product-view">
								<img src="{{ asset('uploads/admin/products')}}/{{$detailProduct->image}}" alt="" height="450">
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="product-body">
							<div class="product-label">
								@if(!empty($detailProduct['sale']))
									<span class="sale">-{{$detailProduct['sale']}}%</span>
								@endif
							</div>
							<h2 class="product-name" title="{{$detailProduct['name']}}">
								{{ the_excerpt($detailProduct['name'], 60) }} 
                                @if(strlen($detailProduct['name'])  > 60) ... @endif 
                            </h2>
                            <br>
							@if(!empty($detailProduct['sale']))

								<?php $price_new = ( $detailProduct['price']*(100-$detailProduct['sale']))/100; ?>
								<h3 class="product-price"><?php echo number_format($price_new) .'đ' ?>
									<del class="product-old-price"><?php echo number_format($detailProduct['price']) .'đ' ?></del>
								</h3>
							@else
								<h3 class="product-price"><?php echo number_format($detailProduct['price']) .'đ' ?>
								</h3>
							@endif

							<p>
								{!!$detailProduct['description']!!}
							</p>

							<div class="product-btns">
								<div class="qty-input">
									<span class="text-uppercase">QTY: </span>
									<input class="input-qty" type="number" min="1" max="{{$detailProduct['total']}}" value="1" style="text-align: center;">
								</div>
								<a href="{{url('mua-hang', $detailProduct['id'])}}" class="btn-add-cart"><button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button></a>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<div class="product-tab">
							<ul class="tab-nav">
								<li class="active"><a data-toggle="tab" href="#tab1">Bài viết mô tả</a></li>
							</ul>
							<div class="tab-content">
								<div id="tab1" class="tab-pane fade in active">
									{!! $detailProduct['contents'] !!}
								</div>
							</div>
						</div>
					</div>

				</div>
				<!-- /Product Details -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
	<!-- /section -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!-- section title -->
				<div class="col-md-12">
					<div class="section-title">
						<h2 class="title">Sản phẩm liên quan</h2>
					</div>
				</div>
				<!-- section title -->

				<!-- Product Single -->
				@if(!empty($products))
					@foreach($products as $product)
						<div class="col-md-4 col-sm-6 col-xs-6">
							<div class="product product-single">
								<div class="product-thumb">
									<div class="product-label">
										<!-- <span>New</span> -->
										@if(!empty($product->sale))
											<span class="sale">-{{$product->sale}}%</span>
										@endif
									</div>
									<a href="{{route('detailProducts',[safe_title($product['name']), $product['id']])}}"><button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Xem Ngay</button>
									</a>
									<img src="{{ asset('uploads/admin/products')}}/{{$product->image}}" alt="">
								</div>
								<div class="product-body">
									@if(!empty($product->sale))

										<?php $price_new = ( $product->price*(100-$product->sale))/100; ?>
										<h3 class="product-price"><?php echo number_format($price_new) .'đ' ?>
											<del class="product-old-price"><?php echo number_format($product->price) .'đ' ?></del>
										</h3>
									@else
										<h3 class="product-price"><?php echo number_format($product->price) .'đ' ?>
										</h3>
									@endif
									<h2 class="product-name">
										<a href="{{route('detailProducts',[safe_title($product['name']), $product['id']])}}" title="{{$product['name']}}">
											{{ the_excerpt($product->name, 50) }}
											@if(strlen($product->name)  > 50) ... @endif
										</a>
									</h2>
									<a href="{{url('mua-hang', $product['id'])}}" class="btn-add-cart"><div class="product-btns">
											<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
										</div></a>
								</div>
							</div>
						</div>
						<!-- /Product Single -->
						<div class="clearfix visible-sm visible-xs"></div>
					@endforeach
				@endif
				<!-- /Product Single -->
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
@endsection